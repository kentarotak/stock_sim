# coding: UTF-8

require "./lib/trade"
require "./lib/array"
require "./lib/stats"
require "fileutils"
require "csv"
require 'kconv'

# 取引の記録を行うクラス
class Recorder
  attr_writer :record_dir

  def initialize(record_dir = nil)
    @record_dir = record_dir
  end

  # 1銘柄の取引を記録する
  def record_a_stock(trades)
    code = trades[0].stock_code
    ensure_close("#{code}.csv") do
      file_name = "#{@record_dir}/#{code}.csv"
      CSV.open(file_name, "w") do |csv_file|
        csv_file << items_for_a_stock.values
        trades.each do |trade|
          one_trade = items_for_a_stock.keys.map do |attr|
            trade.send(attr) || "-"
          end
          csv_file << one_trade
        end
      end
    end
  end

  # 銘柄ごとの統計の一覧表の作成
  def record_stats_for_each_stock(results)
    ensure_close("_stats_for_each_stock.csv") do
      CSV.open("#{@record_dir}/_stats_for_each_stock.csv", "w") do |csv_file|
        csv_file << stats_items.values.unshift("Code")
        results.each do |trades|
          csv_file << stats_array(trades).unshift(trades[0].stock_code)
        end
      end
    end
  end

  # すべてのトレードの統計
  def record_stats(results)
    ensure_close("_stats.csv") do
      CSV.open("#{@record_dir}/_stats.csv", "w") do |csv_file|
        csv_file << stats_items.values
        csv_file << stats_array(results.flatten)
      end
    end
  end

  # 設定ファイルをコピーする
  def record_setting(file_name)
    FileUtils.cp file_name, @record_dir + "/_setting.rb"
  end

  # 結果保存用のフォルダーを作る
  def create_record_folder
    if Dir.exist? @record_dir
      puts "記録フォルダ #{@record_dir} はすでに存在します。上書きしますか？ y/n"
      yes? {puts "上書きします"}
    else
      puts "記録フォルダ #{@record_dir} は存在しません。新しく作りますか？ y/n"
      yes? {FileUtils.mkdir_p @record_dir}
    end
  end

  private
  def items_for_a_stock
    { :trade_type => "trade_type",
      :entry_date => "entry_date",
      :entry_price => "entry_price",
      :volume => "volume",
      :first_stop => "first_stop",
      :exit_date => "exit_date",
      :exit_price => "exit_price",
      :profit => "profit",
      :r_multiple => "r_multiple",
      :percentage_result => "percentage_result",
      :length => "length"}
  end

  def stats_items
    { :sum_profit => "sum_profit",
      :wins => "wins", :losses => "losses",
      :draws => "draws",
      :winning_percentage => "winning_percentage",
      :average_profit => "average_profit",
      :profit_factor => "profit_factor",
      :sum_r => "sum_r", :average_r => "average_r",
      :sum_percentage => "sum_percentage",
      :average_percentage => "average_percentage",
      :average_length => "average_length"}
  end

  def stats_array(trades)
    stats = Stats.new(trades)
    stats_items.keys.map do |stats_name|
      stats.send(stats_name) || "-"
    end
  end

  def ensure_close(file_name)
    begin
      yield
    rescue Errno::EACCES
      puts "#{file_name} が他のプログラムで書き込み禁止で開かれている可能性があります。" +
        "ファイルを閉じてからエンターキーを押してください。"
      while $stdin.gets
        retry
      end
    end
  end

  def yes?
    while answer = $stdin.gets
      if answer =~ /^[yY]/
        yield
        break
      elsif answer =~ /^[nN]/
        puts "終了します"
        exit
      else
        puts "y （はい）か n （いいえ）でお答えください"
      end
    end
  end
end

