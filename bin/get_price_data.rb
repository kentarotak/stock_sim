# coding: UTF-8

require "./lib/stock_data_getter"

# 東証銘柄をダウンロードする
# 名証銘柄をダウンロードしたければ、market = :n とする
# 福証なら market = :f、札証なら market = :s

from = "2010/01/04"
to   = "2010/06/14"
market = :t
sdg = StockDataGetter.new(from, to, market)

(3796..9999).each do |code|
  sdg.get_price_data(code)
end
